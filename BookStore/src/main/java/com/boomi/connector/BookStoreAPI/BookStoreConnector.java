package com.boomi.connector.BookStoreAPI;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;
import com.boomi.util.StringUtil;

public class BookStoreConnector extends BaseConnector {
	protected static final String OFFSET_PARAM = "offset";
	protected static final String LIST_EL_NAME = "list";
	public static final String ECHO_OPERATION_TYPE = "ECHO";

	public Browser createBrowser(BrowseContext context) {
		return (Browser) (this.getCustomOperationType(context).equals("ECHO")
				? new BookStoreBrowser(this.createConnection(context))
				: new BookStoreBrowser(this.createConnection(context)));
	}

	protected Operation createGetOperation(OperationContext context) {
		return new BookStoreGetOperation(this.createConnection(context));
	}

	protected Operation createCreateOperation(OperationContext context) {
		return new BookStoreCreateOperation(this.createConnection(context));
	}

	protected Operation createExecuteOperation(OperationContext context) {
		return (Operation) (this.getCustomOperationType(context).equals("ECHO")
				? new BookStoreEchoOperation(this.createConnection(context))
				: super.createExecuteOperation(context));
	}

	private BookStoreConnection createConnection(BrowseContext context) {
		return new BookStoreConnection(context);
	}

	private String getCustomOperationType(BrowseContext context) {
		return StringUtil.defaultIfBlank(context.getCustomOperationType(), "");
	}
}