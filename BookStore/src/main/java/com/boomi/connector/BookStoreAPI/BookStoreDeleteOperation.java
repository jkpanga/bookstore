package com.boomi.connector.BookStoreAPI;

import com.boomi.connector.api.DeleteRequest;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.util.BaseDeleteOperation;

public class BookStoreDeleteOperation extends BaseDeleteOperation {

	protected BookStoreDeleteOperation(BookStoreConnection conn) {
		super(conn);
	}

	@Override
	protected void executeDelete(DeleteRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public BookStoreConnection getConnection() {
        return (BookStoreConnection) super.getConnection();
    }
}