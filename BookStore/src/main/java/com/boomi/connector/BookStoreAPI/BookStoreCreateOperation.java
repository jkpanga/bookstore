package com.boomi.connector.BookStoreAPI;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Iterator;

public class BookStoreCreateOperation extends BaseUpdateOperation {
	public BookStoreCreateOperation(BookStoreConnection conn) {
		super(conn);
	}

	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		String objectType = this.getContext().getObjectTypeId();
		Iterator i$ = request.iterator();

		while (i$.hasNext()) {
			ObjectData input = (ObjectData) i$.next();

			try {
				BookStoreResponse resp = this.getConnection().doCreate(objectType, input.getData());
				InputStream obj = null;

				try {
					obj = resp.getResponse();
					if (obj != null) {
						response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
								resp.getResponseMessage(), ResponseUtil.toPayload(obj));
					} else {
						response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
								resp.getResponseMessage());
					}
				} finally {
					IOUtil.closeQuietly(new Closeable[]{obj});
				}
			} catch (Exception var12) {
				ResponseUtil.addExceptionFailure(response, input, var12);
			}
		}

	}

	public BookStoreConnection getConnection() {
		return (BookStoreConnection) super.getConnection();
	}
}