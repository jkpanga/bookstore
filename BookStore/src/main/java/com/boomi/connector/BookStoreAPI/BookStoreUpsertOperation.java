package com.boomi.connector.BookStoreAPI;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

public class BookStoreUpsertOperation extends BaseUpdateOperation {

	protected BookStoreUpsertOperation(BookStoreConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public BookStoreConnection getConnection() {
        return (BookStoreConnection) super.getConnection();
    }
}