package com.boomi.connector.BookStoreAPI;


import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.DOMUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.URLUtil;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class BookStoreConnection extends BaseConnection {
	
	private static final List<Entry<String, String>> UPDATE_OUTPUT_PARAMS = Collections
			.unmodifiableList(new ArrayList(Collections.singletonMap("type", "json").entrySet()));
	private final String _baseUrl;

	public BookStoreConnection(BrowseContext context) {
		super(context);
		this._baseUrl = getBaseUrl(context.getConnectionProperties());
	}

	public Document getMetadata(String objectTypeId) throws IOException, SAXException {
		URL url = objectTypeId != null
				? buildUrl(this._baseUrl, "metadata", objectTypeId)
				: buildUrl(this._baseUrl, "metadata");
		return parse(url.openStream());
	}

//	public BookStoreResponse doGet(String objectType, String objectId) throws IOException {
//		String doGeturl = this._baseUrl + "/book/getAllBooks";
//		URL url = buildUrl(doGeturl, objectType, objectId);
//		return new BookStoreResponse((HttpURLConnection) url.openConnection());
//	}

	
	public BookStoreResponse doGet(String objectType, String objectId) throws IOException {
		String doGeturl = this._baseUrl + "/book/getAllBooks";
		//URL url = buildUrl(doGeturl, objectType, objectId);
		URL url = new URL(doGeturl);
		return new BookStoreResponse((HttpURLConnection) url.openConnection());
	}


	public BookStoreResponse doCreate(String objectType, InputStream data) throws IOException {
		String doCreateurl = this._baseUrl + "/book/addNewBooks";
		return new BookStoreResponse(
				send(buildUrl(UPDATE_OUTPUT_PARAMS, doCreateurl, objectType), "POST", "application/json", data));
	}

	public BookStoreResponse doDelete(String objectType, String objectId) throws IOException {
		URL url = buildUrl(this._baseUrl, objectType, objectId);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("DELETE");
		return new BookStoreResponse(conn);
	}

	private static String getBaseUrl(PropertyMap props) {
		return props.getProperty("url");
	}
	
	private static Boolean getAuthKey(PropertyMap props) {
		return props.getBooleanProperty("Authentication");
	}

	private static Document parse(InputStream input) throws SAXException, IOException {
		Document var1;
		try {
			var1 = DOMUtil.newDocumentBuilderNS().parse(input);
		} finally {
			IOUtil.closeQuietly(new Closeable[]{input});
		}

		return var1;
	}

	private static URL buildUrl(String... components) throws IOException {
		return buildUrl((List) null, components);
	}

	private static URL buildUrl(List<Entry<String, String>> params, String... components) throws IOException {
		return URLUtil.makeUrl(params, (Object[]) components);
	}

	private static HttpURLConnection send(URL url, String requestMethod, String contentType, InputStream data)
			throws IOException {
		HttpURLConnection var6;
		try {
			HttpURLConnection conn = prepareSend(url, requestMethod, contentType);
			OutputStream out = conn.getOutputStream();

			try {
				StreamUtil.copy(data, out);
			} finally {
				out.close();
			}

			var6 = conn;
		} finally {
			IOUtil.closeQuietly(new Closeable[]{data});
		}

		return var6;
	}

	private static HttpURLConnection prepareSend(URL url, String requestMethod, String contentType) throws IOException {
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(requestMethod);
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", contentType);
		return conn;
	}
}