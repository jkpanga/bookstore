package com.boomi.connector.BookStoreAPI;


import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.util.BaseBrowser;
import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class BookStoreBrowser extends BaseBrowser {
	private static final String TYPE_ELEMENT = "type";

	public BookStoreBrowser(BookStoreConnection conn) {
		super(conn);
	}

	public ObjectTypes getObjectTypes() {
		try {
			Document typeDoc = this.getConnection().getMetadata((String) null);
			NodeList typeList = typeDoc.getElementsByTagName("type");
			ObjectTypes types = new ObjectTypes();

			for (int i = 0; i < typeList.getLength(); ++i) {
				Element typeEl = (Element) typeList.item(i);
				String typeName = typeEl.getTextContent().trim();
				ObjectType type = new ObjectType();
				type.setId(typeName);
				types.getTypes().add(type);
			}

			return types;
		} catch (Exception var8) {
			throw new ConnectorException(var8);
		}
	}

	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
		try {
			Document defDoc = this.getConnection().getMetadata(objectTypeId);
			ObjectDefinitions defs = new ObjectDefinitions();
			ObjectDefinition def = new ObjectDefinition();
			def.setSchema(defDoc.getDocumentElement());
			def.setElementName(objectTypeId);
			defs.getDefinitions().add(def);
			return defs;
		} catch (Exception var6) {
			throw new ConnectorException(var6);
		}
	}

	public BookStoreConnection getConnection() {
		return (BookStoreConnection) super.getConnection();
	}
}